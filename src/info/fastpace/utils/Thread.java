package info.fastpace.utils;

public class Thread extends java.lang.Thread {

	public Thread(Runnable runnable) {
		super(runnable);
	}

	public static Thread runInNewThread(Runnable runnable) {
		Thread thread = new Thread(runnable);
		thread.start();
		return thread;
	}
}
