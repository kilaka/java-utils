package info.fastpace.utils;


public class RunnnableDecorator<R extends Runnable> extends Decorator<R> implements Runnable {

	public RunnnableDecorator(R runnable) {
		super(runnable);
	}

	@Override
	public void run() {
		getInner().run();
	}

	public static class RunnableDecoratorSimple extends RunnnableDecorator<Runnable> {
		public RunnableDecoratorSimple(Runnable runnable) {
			super(runnable);
		}
	}
}
