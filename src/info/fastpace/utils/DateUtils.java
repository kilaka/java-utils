package info.fastpace.utils;

import java.util.Calendar;

/**
 * 
 * @author EliDev
 *
 */
public class DateUtils {

	/**
	 * 
	 * @param origtime
	 * @return 
	 */
	public static long getMonth(long origtime) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(origtime);
		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);
		calendar.clear();
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.YEAR, year);
		return calendar.getTimeInMillis();
	}
	
	/***
	 * 
	 * @param c1  Accepts a calendar instance
	 * @return a string with the GMT hours.
	 */
	public static String getRealGMT(Calendar c1){
		int gmtNumber=0;
		System.out.println("c1 "+c1.getTime().toString());
		
		if (c1.getTime().toString().contains("+")) {
			
			gmtNumber = Integer.parseInt(c1.getTime().toString().substring(c1.getTime().toString().indexOf("+")+1, c1.getTime().toString().indexOf("+")+3));
			if ( gmtNumber/10 == 0) return "+0"+gmtNumber;
			else return Integer.toString(gmtNumber);
		}

		if (c1.getTime().toString().contains("-")) {
			gmtNumber = Integer.parseInt(c1.getTime().toString().substring(c1.getTime().toString().indexOf("-")+1, c1.getTime().toString().indexOf("-")+2));
			if ( gmtNumber/10 == 0) return "-0"+gmtNumber;
			else return Integer.toString(-1*gmtNumber);
		}
        
		return "00";
	}
	
	public static long getGMTHoursDifferenceInMilliseconds(long dateInMilliseconds){
		Calendar c1 = Calendar.getInstance();
		c1.setTimeInMillis(dateInMilliseconds);
		long difference=Long.parseLong((getRealGMT(c1)))*3600000;
		return difference;
	}

}
