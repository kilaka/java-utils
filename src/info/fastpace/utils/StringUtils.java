package info.fastpace.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class StringUtils {
	
	public static final String DEFAULT_ENCODING = "UTF-8";

	public static String readLine(InputStream inputStream) throws IOException {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		int c;
		for (c = inputStream.read(); c != '\n' && c != -1 ; c = inputStream.read()) {
			byteArrayOutputStream.write(c);
		}
		if (c == -1 && byteArrayOutputStream.size() == 0) {
			return null;
		}
		String line = byteArrayOutputStream.toString(DEFAULT_ENCODING);
		return line;
	}
	
//	public static String readLine(InputStream inputStream) {
////		return substringBefore(rpcAsString, "\n");
//		Scanner scanner = new Scanner(inputStream, "UTF-8");
//		String line = scanner.nextLine();
//		return line;
//	}
	
    public static String substringBefore(final String str, final String separator) {
        if (isEmpty(str) || separator == null) {
            return str;
        }
        if (separator.isEmpty()) {
            return "";
        }
        final int pos = str.indexOf(separator);
        if (pos == -1) {
            return str;
        }
        return str.substring(0, pos);
    }
    
    public static boolean isEmpty(final CharSequence cs) {
        return cs == null || cs.length() == 0;
    }

//	public static String readLine(Reader reader) throws IOException {
//		StringBuilder stringBuilder = new StringBuilder();
//		int c;
//		for (c = reader.read(); c != '\n' && c != -1 ; c = reader.read()) {
//			stringBuilder.append((char)c);
//		}
//		if (c == -1 && stringBuilder.length() == 0) return null; // End of stream and nothing to return
//		return stringBuilder.toString();
//	}
}
