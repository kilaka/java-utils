package info.fastpace.utils;

import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class CancelableTask extends Observable<CancelableTask>
		implements Runnable {
	private volatile boolean isStarted = false;
	private volatile boolean isCanceled = false;
	private volatile boolean isSuccess = false;
	private volatile Exception e;
	private volatile AtomicBoolean doneLock = new AtomicBoolean(false);
	protected final AtomicInteger progress = new AtomicInteger(0);
	private final boolean sync;
	private long startTime = 0;
	private long endTime = 0;
	private CancelableTask inner;

	public CancelableTask() {
		this(true);
	}
	
	public CancelableTask(boolean sync) {
		this.sync = sync;
	}
	
	/**
	 * Runs the provided task as part of this task.
	 * This means that it's part of the lifecycle of the outer task in terms of exceptions and cancelations.
	 */
	protected void runNow(CancelableTask task) throws Exception {
		if (inner != null) throw new Exception("Inner task already running.");
		inner = task;
		inner.addObserver(new Observer<CancelableTask>() {
			@Override
			public void update(CancelableTask args) {
				notifyObservers(CancelableTask.this);
				if (inner == null || !inner.isDone()) return;
				inner = null;
			}
		});
		inner.runUnsafe();
	}

	@Override
	public final void run() {
		try {
			runUnsafe();
		} catch (CancellationException e) {
			// Ignore - natural behavior to cancel while still running
		} catch (Exception e) {
			Config.getLog().e("CancelableTask caught an exception", e);			
		}
	}

	public final void runUnsafe() throws Exception {
		startTime = System.currentTimeMillis();
//		Config.getLog().d("Running cancelable task: " + toString());
		notifyObservers(this);
		isStarted = true;
		try {
			if (!isCanceled) {
				runImpl();
			}
		} catch (Exception e) {
			// Note: Cancel may throw exception but the done lock would be checked!!!
			markFailure(e);
			throw e;
			// Cancel got to the lock first but may NOT have yet changed the cancel flag.
			// Must throw cancellation exception.
		}
		if (sync) {
			markSuccess();
			if (isSuccess) return;

			// The task was canceled but the isCanceled may not have been set yet.
			
			synchronized (doneLock) { // Waiting for the cancel to finish it's logic
			}
	
	//		assert isCanceled; // Commented out because android crashes the app in assertion 
			// No need to notify here because cancel was already notified in
			// cancel method.
			// notifyObservers(this);
	//		Config.getLog().d("Already canceled task: " + toString());
			throw new CancellationException("Canceled while running!");
		}
	}

	protected abstract void runImpl() throws Exception;

	protected final void markSuccess() {
		if (doneLock.compareAndSet(false, true)) {
			endTime  = System.currentTimeMillis();
			isSuccess = true;
			progress.set(100);
			notifyObservers(this);
			clearObservers();
		}
	}

	protected final void markFailure(Exception e) {
		if (doneLock.compareAndSet(false, true)) {
			endTime  = System.currentTimeMillis();
			this.e = e;
			notifyObservers(this);
			clearObservers();
			Config.getLog().w("CancelableTask caught an exception", e);			
		}
	}

	protected void cancelImpl() {}

	public final void cancel() {
//		boolean cancelGotLock = false;
		synchronized (doneLock) {
//			cancelGotLock = doneLock.compareAndSet(false, true)
			if (doneLock.compareAndSet(false, true)) {
				endTime  = System.currentTimeMillis();
//				Config.getLog().d("Canceling cancelable task: " + toString());
				isCanceled = true;
				
				if (inner != null) inner.cancel();

				cancelImpl();
				
				notifyObservers(this);

				clearObservers();
			}
		}
	}

	public final boolean isCanceled() {
		return isCanceled;
	}

	public final boolean isSuccessful() {
		return isSuccess;
	}

	public final boolean isDone() {
		return doneLock.get();
	}

	public final boolean isStarted() {
		return isStarted;
	}

	public final Exception getError() {
		return e;
	}

	public int getProgress() {
		return progress.get();
	}

	/**
	 * Observers will be cleared after the task is done but only after all of them are notified.
	 */
	@Override
	public void addObserver(Observer<CancelableTask> observer) {
		super.addObserver(observer);
	}

	public long getStartTime() {
		return startTime;
	}

	public long getEndTime() {
		return endTime;
	}
	
	
//	protected void incrementProgress(int value) {
//		progress += value;
//	}
}
