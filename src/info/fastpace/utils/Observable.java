package info.fastpace.utils;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Observable<T> {

	private ConcurrentLinkedQueue<Observer<T>> observers = new ConcurrentLinkedQueue<Observer<T>>();
	
	public void addObserver(Observer<T> observer) {
		observers.add(observer);
	}
	
	public boolean removeObserver(Observer<T> observer) {
		return observers.remove(observer);
	}
	
	public void notifyObservers(T arg) {
		for (Observer<T> observer : observers) {
			observer.update(arg);
		}
	}
	
	public Collection<Observer<T>> getObservers() {
		return Collections.unmodifiableCollection(observers);
	}
	
	public void clearObservers() {
		observers.clear();
	}
	
//	public void clearObservers() {
//		observers.clear();
//	}
}
