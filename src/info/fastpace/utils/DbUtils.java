package info.fastpace.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DbUtils {

	public static void closeQuietly(Connection connection) {
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (Exception ioe) {
			// ignore
		}
	}

	public static int updateOrInsert(PreparedStatement updateStatement, PreparedStatement insertStatement) throws SQLException {
		int rowsUpdated = updateStatement.executeUpdate();
		if (rowsUpdated == 0) {
			rowsUpdated = insertStatement.executeUpdate();
		}
		return rowsUpdated;
	}

}
