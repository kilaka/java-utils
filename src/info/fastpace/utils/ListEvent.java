package info.fastpace.utils;

import java.util.Collection;


@SuppressWarnings("serial")
public class ListEvent<E> extends CollectionEvent<E> {

	private final int index;
	
	public ListEvent(Collection<E> source, Operation operation) {
		super(source, operation);
		this.index = -1;
	}

	public ListEvent(Collection<E> source, E element, int index, Operation operation) {
		super(source, element, operation);
		this.index = index;
	}

	public ListEvent(Collection<E> source, Collection<? extends E> elements, 
			int index, Operation operation) {
		super(source, elements, operation);
		this.index = index;
	}

	public ListEvent(Collection<E> source, E oldElement, E newElement, int location) {
		super(source, oldElement, newElement);
		this.index = location;
	}

	public int getIndex() {
		return index;
	}
}
