package info.fastpace.utils;


public interface Observer<T> {
	
	public void update(T args);
	
	

}
