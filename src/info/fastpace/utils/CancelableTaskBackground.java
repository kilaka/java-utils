package info.fastpace.utils;

import java.util.concurrent.Semaphore;

public abstract class CancelableTaskBackground extends CancelableTask {
	private final Semaphore lock = new Semaphore(0);
	private volatile Exception exception;
	
	public CancelableTaskBackground() {}
	
	@Override
	protected void runImpl() throws Exception {
		Thread.runInNewThread(new Runnable() {
			@Override
			public void run() {
				try {
					runBackground();
				} catch (Exception e) {
					CancelableTaskBackground.this.exception = e;
				}finally {
					lock.release();
				}
			}
		});
		lock.acquireUninterruptibly();
		if (exception != null) {
			throw exception;
		}
	}

	protected abstract void runBackground() throws Exception;
}
