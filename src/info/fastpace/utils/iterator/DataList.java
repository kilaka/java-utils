package info.fastpace.utils.iterator;

import info.fastpace.utils.CollectionEvent;
import info.fastpace.utils.CollectionListener;
import info.fastpace.utils.ObservableList;
import info.fastpace.utils.CollectionEvent.Operation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;


public class DataList<T> extends ObservableList<T> implements Serializable {
	private static final long serialVersionUID = 5332759228136214733L;

	private final Iterator<? extends T> iterator; //the iterator

	private final Loader loader = new Loader();
	private volatile AtomicBoolean isRunning = new AtomicBoolean(false);
	private volatile AtomicInteger largestRequestedIndex = new AtomicInteger(75);
	private volatile boolean iteratorHasMore = true;
//	private volatile AtomicBoolean loadingDoneSent = new AtomicBoolean(false); // Send done loading message only once

	private boolean wakupRequested = false;

	public DataList(Iterator<? extends T> iterator) {
		this(iterator, -1);
//		this.chunk = chunk;
//		this.iterator = iterator;

//		largestRequestedIndex.set(3);
//		largestRequestedIndex.set(position >= 0 ? position : 0);
//		// Load in the forground up to position.
//		while (position-- >= 0 && needToLoadMore()) {
//			T a = iterator.next();
//			if (a != null) {
//				add(a);
//			}
//		}

//		wakeup();
	}

	public DataList(Iterator<? extends T> iterator, int position) {
		super(new ArrayList<T>());
		this.iterator = iterator;
		largestRequestedIndex.set(Math.max(position, largestRequestedIndex.get()));
		while (position-- >= 0 && needToLoadMore()) {
			T a = iterator.next();
			if (a != null) {
				add(a);
			}
		}
		wakeup();
	}

	@Override
	public T get(int index) {
//		System.err.println("Requested index: " + index);
		T item = super.get(index); // Do it first to check IOOB
		
		// Update largest requested index
		int largestRequestedIndex = this.largestRequestedIndex.get();
		while (index > largestRequestedIndex) {
			if (this.largestRequestedIndex.compareAndSet(largestRequestedIndex, index)) {
//				Config.getLog().d("largestRequestedIndex updated to:" + this.largestRequestedIndex.get());
				break;
			}
			largestRequestedIndex = this.largestRequestedIndex.get();
		}
		
		tryToWakeup();

		return item;
	}

	private boolean needToLoadMore() {
		if (!iteratorHasMore) {
			return false;
		}
		if (!iterator.hasNext()) {
			iteratorHasMore = false;
			return false;
		}
		return isRequestedIndexLarge();
	}


	private boolean isRequestedIndexLarge() {
		int size = size();
		int largestRequestedIndexInt = largestRequestedIndex.get();
		boolean isRequestedIndexLarge = (largestRequestedIndexInt)*4/3 >= size;
		return isRequestedIndexLarge;
	}

	public boolean doneLoading() {
		return !iteratorHasMore; // No more to load and all were added
	}

	protected void tryToWakeup() {
		if (iteratorHasMore && isRequestedIndexLarge()) {
			wakeup();
		}
		
	}

	protected void wakeup() {
		wakupRequested  = true;
		if (isRunning.compareAndSet(false, true)) { // requested index exceeds delta from top and not running, thus need to run and load more.
			new Thread(loader).start();
		}
	}

	@Override
	public boolean add(T item) {
		boolean added = super.add(item);
		
		trySendLodingDoneMessage();
		
		return added;
	}
	
	@Override
	public boolean addAll(Collection<? extends T> elems) {
		boolean retval = super.addAll(elems);

		trySendLodingDoneMessage();

		return retval;
	}

	private void trySendLodingDoneMessage() {
//		if (doneLoading() && loadingDoneSent.compareAndSet(false, true)) {
		if (doneLoading()) {
			//send message done
			firePostEvent(null);
		}
	}

	@Override
	public Iterator<T> iterator() {
		return new NonConcurrentIterator();
	}
	
	

	@Override
	public T remove(int index) {
		T removed = super.remove(index);
		return removed;
	}

	@Override
	public boolean remove(Object object) {
		boolean removed = super.remove(object);
		return removed;
	}



	/**
	 * The iterator's only purpose is to prevent throwing {@link ConcurrentModificationException}
	 * It is VERY not concurrent.
	 * It assumes that by the time this iterator is used, remove will not be called, because remove can cause IOOBE or skip elements.
	 * Anyhow, it's better than throwing CME (ConcurrentModificationException)
	 */
    private class NonConcurrentIterator implements Iterator<T> {
    	private volatile int lastSize = 0;
    	private volatile int nextPosition = 0;

		@Override
		public boolean hasNext() {
			if (nextPosition < size()) {
				// Has an element
				return true;
			}
			if (doneLoading()) {
				// if done loading, perhaps there is another element
				return nextPosition < size();
			}
			final Semaphore lock = new Semaphore(0);
			CollectionListener<T> collectionListener = new CollectionListener<T>() {
				@Override
				public void pre(CollectionEvent<T> event) {
				}
				@Override
				public void post(CollectionEvent<T> event) {
					if (doneLoading()
						|| (event != null && event.getOperation().equals(Operation.ADD)
							&& lastSize != size() // protect against locking on old size
							)
					){
						removeCollectionListener(this);
						lock.release();
					}
				}
			};
			lastSize = size();
			addCollectionListener(collectionListener);
			if (nextPosition < size()) {
				removeCollectionListener(collectionListener);
				// Has an element
				return true;
			}
			if (doneLoading()) {
				removeCollectionListener(collectionListener);
				return nextPosition < size();
			}
			lock.acquireUninterruptibly();
			return nextPosition < size();
		}

		@Override
		public T next() {
			if (!hasNext()) {
				throw new NoSuchElementException("No more elements");
			}

			return get(nextPosition++);
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
    }

	/**
	 * the thread that loads information in the background
	 */
	private class Loader implements Runnable {
		@Override
		public void run() {
			// Assert I have the permission
			//send message start
			firePostEvent(null);
			do {
				List<T> tmp = new LinkedList<T>();
				int i=0;
				while (i++ < PagingIterator.FIRST_PAGE_SIZE && needToLoadMore()) {
					T a = iterator.next();
					tmp.add(a);
				}
				addAll(tmp);
			} while (needToLoadMore()); // Need more
			wakupRequested = false;
			isRunning.set(false);
			if (wakupRequested) wakeup();
//			Config.getLog().d("Loading iteration finished.");
		}
	}
}
