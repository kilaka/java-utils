package info.fastpace.utils.iterator;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class IndexBasedIterator<T> implements Iterator<T> {
	private volatile int nextPosition = 0;
	private List<? extends T> list;

	public IndexBasedIterator(List<? extends T> list) {
		this.list = list;
	}
	
	@Override
	public boolean hasNext() {
		return nextPosition < list.size();
	}

	@Override
	public T next() {
		if (!hasNext()) {
			throw new NoSuchElementException("No more elements");
		}

		return list.get(nextPosition++);
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
