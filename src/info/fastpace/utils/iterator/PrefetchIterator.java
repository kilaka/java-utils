package info.fastpace.utils.iterator;

import java.util.Iterator;
import java.util.NoSuchElementException;

public abstract class PrefetchIterator<E> implements Iterator<E> {

	private E next;
	private boolean finished = false;

	public PrefetchIterator() {
	}

	/**
	 * Comparator here is used just to check equality. No real need to say if it's bigger or smaller. If I had some king of an interface 'Equalator', I would have used.
	 */
	@Override
	public final boolean hasNext() {
		if (finished) return false;
		if (next == null) {
			next = prefetch();
			if (next == null) {
				finished = true;
			}
		}
		return next != null;
	}

	protected abstract E prefetch();

	@Override
	public E next() {
		if (!hasNext()) {
			throw new NoSuchElementException("No more elements");
		}
		E next = this.next;
		this.next = null;
		return next;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException("Remove method not supported");
	}

}
