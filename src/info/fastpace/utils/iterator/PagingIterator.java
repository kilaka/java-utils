package info.fastpace.utils.iterator;

import java.util.List;

public abstract class PagingIterator<E> extends ConsecutiveIterator<E> {
	
	public static final int FIRST_PAGE_SIZE = 25;
	
	private int offset = 0;
	private int pageSize;
	private int pageSizeInitial;
	private boolean finished = false;

	public PagingIterator() {
		this(FIRST_PAGE_SIZE, 250);
	}

	public PagingIterator(int pageSizeInitial, int pageSize) {
		this.pageSizeInitial = pageSizeInitial;
		this.pageSize = pageSize;
	}

	@Override
	public final boolean hasNext() {
		if (super.hasNext()) {
			return true;
		}
		if (finished) {
			return false;
		}
		int size = offset == 0 ? pageSizeInitial : pageSize;
		List<E> list = getPage(offset, size);
		offset += size;
		
		if (list.isEmpty()) {
			finished = true;
			return false;
		}

		add(list.iterator());

		return super.hasNext();
	}

	/**
	 * Empty iterator signifies no more pages
	 * 
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	protected abstract List<E> getPage(int offset, int pageSize);
}
