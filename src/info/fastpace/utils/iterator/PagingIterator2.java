package info.fastpace.utils.iterator;

import java.util.Iterator;

public class PagingIterator2<E> implements Iterator<E> {
	
	private final Iterator<Iterator<E>> pagesIterator;
	private Iterator<E> current;

	public PagingIterator2(Iterator<Iterator<E>> pagesIterator) {
		this.pagesIterator = pagesIterator;
	}

	@Override
	public final boolean hasNext() {
//		if (current == null) {
//			if (!pagesIterator.hasNext()) {
//				return false;
//			}
//			current = pagesIterator.next();
//		}
		while ((current == null || !current.hasNext()) && pagesIterator.hasNext()) {
			current = pagesIterator.next();
		}
		return current != null && current.hasNext();
	}

	@Override
	public E next() {
		return current.next();
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException("Remove method not supported");
	}
}
