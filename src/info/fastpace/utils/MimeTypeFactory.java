package info.fastpace.utils;


public interface MimeTypeFactory {
	MimeType getMimeTypeFromFilename(String filename);

	class Util {
		private Util() {
		}
		public static class Decorator<F extends MimeTypeFactory> extends
				info.fastpace.utils.Decorator<F> implements
				MimeTypeFactory {
			public Decorator(F feature) {
				super(feature);
			}

			@Override
			public MimeType getMimeTypeFromFilename(String filename) {
				return getInner().getMimeTypeFromFilename(filename);
			}
		}
	}
}
