package info.fastpace.utils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ReflectionUtil {
	public static Class<?>[] getAssignableInterfaces(Object object) {
		return getAssignableInterfaces(object.getClass());
	}
	
	public static Class<?>[] getAssignableInterfaces(Class<?> clazz) {
		Set<Class<?>> set = new HashSet<Class<?>>();
		for (;clazz != null;clazz=clazz.getSuperclass()) {
			Class<?>[] interfaces = clazz.getInterfaces();
			set.addAll(Arrays.asList(interfaces));
			// This is not the fastest way, but it's relatively clean
			for (Class<?> iface : interfaces) {
				set.addAll(Arrays.asList(getAssignableInterfaces(iface)));
			}
		}
		
		return set.toArray(new Class<?>[set.size()]);
	}
	
	private ReflectionUtil(){}
}
