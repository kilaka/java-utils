package info.fastpace.utils;

public class Config {

	private volatile static Log log = new Log.Util.DefaultLog();

	private static String USERAGENT = null;

	@SuppressWarnings("unchecked")
	public static void setLogClass(String logClass) {
		Class<Log> clazz;
		try {
			clazz = (Class<Log>) Class.forName(logClass);
		} catch (Exception e) {
			String msg = "Error finding log implementor named: " + logClass;
			log.e(msg, e);
			return;
		}
		setLogClass(clazz);
	}
	public static void setLogClass(Class<? extends Log> logClass) {
		try {
			log = logClass.newInstance();
		} catch (Exception e) {
			log.e("Error initiating log class: " + logClass.getName(), e);
			return;
		}
		log.d("Logger changed to: " + log.toString());
	}

	public static Log getLog() {
		return log;
	}

	public static String getUseragent() {
		return USERAGENT;
	}
	
	public static void setUseragent(String useragent) {
		USERAGENT = useragent;
	}
}
