package info.fastpace.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

public interface Log {

	public void d(String msg, Throwable tr);

	public void d(String msg);

	public void i(String msg);

	public void i(String msg, Throwable tr);

	public void w(String msg);

	public void w(String msg, Throwable tr);

	public void e(String msg);

	public void e(String msg, Throwable tr);

	public class Util {
		private Util() {
		}

		public static class EmptyLog implements Log {
			public void d(String msg, Throwable tr) {
			}

			public void d(String msg) {
			}

			public void i(String msg) {
			}

			public void i(String msg, Throwable tr) {
			}

			public void w(String msg) {
			}

			public void w(String msg, Throwable tr) {
			}

			public void e(String msg) {
			}

			public void e(String msg, Throwable tr) {
			}
		}

		public static class DefaultLog implements Log {
			@Override
			public void d(String msg, Throwable tr) {
				Logger.getAnonymousLogger().log(Level.FINE, msg, tr);
			}

			@Override
			public void d(String msg) {
				Logger.getAnonymousLogger().log(Level.FINE, msg);
			}

			@Override
			public void i(String msg) {
				Logger.getAnonymousLogger().log(Level.INFO, msg);
			}

			@Override
			public void i(String msg, Throwable tr) {
				Logger.getAnonymousLogger().log(Level.INFO, msg, tr);
			}

			@Override
			public void w(String msg) {
				Logger.getAnonymousLogger().log(Level.WARNING, msg);
			}

			@Override
			public void w(String msg, Throwable tr) {
				Logger.getAnonymousLogger().log(Level.WARNING, msg, tr);
			}

			@Override
			public void e(String msg) {
				Logger.getAnonymousLogger().log(Level.SEVERE, msg);
			}

			@Override
			public void e(String msg, Throwable tr) {
				Logger.getAnonymousLogger().log(Level.SEVERE, msg, tr);
			}

		}
	}

}
