package info.fastpace.utils;

import java.util.Comparator;

public class ComparatorDecorator<T> extends Decorator<Comparator<T>> implements Comparator<T>{

	public ComparatorDecorator(Comparator<T> t) {
		super(t);
	}

	@Override
	public int compare(T lhs, T rhs) {
		return getInner().compare(lhs, rhs);
	}

	public static class Util {
		private Util() {}
		public static class ComparatorReverse<T> extends ComparatorDecorator<T> {
			public ComparatorReverse(Comparator<T> t) {
				super(t);
			}
			@Override
			public int compare(T lhs, T rhs) {
				return -1 * super.compare(lhs, rhs);
			}

			
		}
	}
}
