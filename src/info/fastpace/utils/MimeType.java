package info.fastpace.utils;

import java.net.FileNameMap;
import java.net.URLConnection;

import org.apache.commons.io.FilenameUtils;

public class MimeType {
	private static MimeTypeFactory mimeTypeFactory;
	private String mimetype = "*/*"; // Default
	private String prefix = "*";
	
	public static final MimeType ALL = new MimeType("*/*");
	public static final MimeType MUSIC_ALL = new MimeType("audio/*");
	public static final MimeType IMAGES_ALL = new MimeType("image/*");
	public static final MimeType VIDEOS_ALL = new MimeType("video/*");
	
	static {
		setMimeTypeFactory(new MimeTypeFactoryImp());	
	}

	public MimeType(String mimetype) {
		super();
		if (mimetype == null) {
			return;
		}

		int indexOfSlash = mimetype.indexOf('/');
		if (indexOfSlash == -1) {
			return;
		}
		this.mimetype = mimetype;
		this.prefix = mimetype.substring(0, indexOfSlash);
	}

	public String getMimetype() {
		return mimetype;
	}
	
	public String getPrefix() {
		return prefix;
	}

	public boolean isImage() {
		return prefix.equals(IMAGES_ALL.getPrefix());
	}

	public boolean isVideo() {
		return prefix.equals(VIDEOS_ALL.getPrefix());
	}

	public boolean isAudio() {
		return prefix.equals(MUSIC_ALL.getPrefix());
	}

	@Override
	public String toString() {
		return mimetype;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mimetype == null) ? 0 : mimetype.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MimeType other = (MimeType) obj;
		if (mimetype == null) {
			if (other.mimetype != null)
				return false;
		} else if (!mimetype.equals(other.mimetype))
			return false;
		return true;
	}
	
	public static void setMimeTypeFactory(MimeTypeFactory mimeTypeFactory) {
		MimeType.mimeTypeFactory = new MimeTypeFactory.Util.Decorator<MimeTypeFactory>(mimeTypeFactory) {
			@Override
			public MimeType getMimeTypeFromFilename(String filename) {
				String extension = FilenameUtils.getExtension(filename);
				if (extension != null) {
					extension = extension.toLowerCase();
					if (extension.equals("mkv")) {
						return new MimeType("video/x-matroska");
					}
				}
				return super.getMimeTypeFromFilename(filename);
			}
		};
	}

	public static MimeType getMimeTypeFromFilename(String filename) {
		return mimeTypeFactory.getMimeTypeFromFilename(filename);
	}
	
	public static class MimeTypeFactoryImp implements MimeTypeFactory {
		@Override
		public MimeType getMimeTypeFromFilename(String filename) {
			FileNameMap fileNameMap = URLConnection.getFileNameMap();
	    	String mimetypeString = fileNameMap.getContentTypeFor(filename);
			MimeType mimeType = new MimeType(mimetypeString);
			return mimeType;
		}
		
	}
	
	public static boolean isMedia(String filename) {
		MimeType mimeType = MimeType.getMimeTypeFromFilename(filename);
		if (mimeType.isAudio() || mimeType.isImage() || mimeType.isVideo()) {
			return true;
		}
		return false;
	}

	
}
