package info.fastpace.utils;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/**
 * Does not allow modification through iterator
 * 
 * @author alik
 * 
 * @param <E>
 */
public class ObservableList<E> extends ObservableCollection<E> implements List<E> {

	protected class ObservableListIterator extends ObservableIterator implements ListIterator<E> {

		private ListIterator<E> decorated;
		private E current;
		private int currentIndex = -1;

		public ObservableListIterator(ListIterator<E> decorated) {
			super(decorated);
			this.decorated = decorated;
		}

		public void add(E elem) {
			ListEvent<E> evt = new ListEvent<E>(ObservableList.this, current, currentIndex, CollectionEvent.Operation.ADD);
			firePreEvent(evt);
			decorated.add(elem);
			firePostEvent(evt);						
		}

		public boolean hasPrevious() {
			return decorated.hasPrevious();
		}

		public int nextIndex() {
			return decorated.nextIndex();
		}

		public E next() {
			currentIndex++;
			return super.next();
		}

		public E previous() {
			currentIndex--;
			current = decorated.previous();
			return current;
		}

		public int previousIndex() {
			return decorated.previousIndex();
		}

		public void set(E elem) {
			ListEvent<E> evt = new ListEvent<E>(ObservableList.this, elem, currentIndex, CollectionEvent.Operation.UPDATE);
			firePreEvent(evt);
			decorated.set(elem);
			firePostEvent(evt);						
		}
	}

	public ObservableList(List<E> decorated) {
		super(decorated);
	}

	@Override
	public List<E> getDecorated() {
		return (List<E>) super.getDecorated();
	}

	public void add(int location, E elem) {
		ListEvent<E> evt = new ListEvent<E>(this, elem, location, CollectionEvent.Operation.ADD);
		firePreEvent(evt);
		getDecorated().add(location, elem);
		firePostEvent(evt);
	}

	public boolean addAll(int location, Collection<? extends E> c) {
		ListEvent<E> evt = new ListEvent<E>(this, c, location, CollectionEvent.Operation.ADD);
		firePreEvent(evt);
		boolean retval = getDecorated().addAll(location, c);
		if (retval)
			firePostEvent(evt);
		return retval;
	}

	public E get(int location) {
		return getDecorated().get(location);
	}

	public int indexOf(Object object) {
		return getDecorated().indexOf(object);
	}

	public int lastIndexOf(Object object) {
		return getDecorated().lastIndexOf(object);
	}

	public ListIterator<E> listIterator() {
		return new ObservableListIterator(getDecorated().listIterator());
	}

	public ListIterator<E> listIterator(int location) {
		return new ObservableListIterator(getDecorated().listIterator(location));
	}

	public E remove(int location) {
		E elem = getDecorated().get(location);
		ListEvent<E> evt = new ListEvent<E>(this, elem, location, CollectionEvent.Operation.REMOVE);
		firePreEvent(evt);
		E retval = getDecorated().remove(location);
		firePostEvent(evt);
		return retval;
	}

	public E set(int location, E elem) {
		ListEvent<E> evt = new ListEvent<E>(this, getDecorated().get(location), elem, location);
		firePreEvent(evt);
		E retval = getDecorated().set(location, elem);
		firePostEvent(evt);
		return retval;
	}

	public List<E> subList(int start, int end) {
		throw new RuntimeException("Not supported in ObservableList");
	}
}
