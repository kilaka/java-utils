package info.fastpace.utils;

public class Decorator<T> {
	private final T inner;

	public Decorator(T t) {
		super();
		this.inner = t;
	}

	protected T getInner() {
		return inner;
	}

	public boolean equals(Object o) {
		return o == this || getInner().equals(o);
	}

	public int hashCode() {
		return inner.hashCode();
	}
}
