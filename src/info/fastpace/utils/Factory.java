package info.fastpace.utils;


public interface Factory<T> {
	public T create();
}