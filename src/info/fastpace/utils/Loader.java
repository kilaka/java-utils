package info.fastpace.utils;

import java.util.concurrent.Executor;

public abstract class Loader<T> extends CancelableTask implements Factory<T>{
	private T result;
//	private Observer<T> observer;
	private final Loader<T> loader;
	private final Executor executor;
	
	public Loader() {
		this(true, null, null);
	}

	public Loader(Loader<T> loader) {
		this(true, loader, null);
	}
	public Loader(Loader<T> loader, Executor executor) {
		this(false, loader, executor);
	}

	private Loader(boolean sync, Loader<T> loader, Executor executor) {
		super(sync);
		this.loader = loader;
		this.executor = executor;
	}

	@Override
	protected final void runImpl() throws Exception {
		try {
			result = create();
		} catch (Exception e){
			if (loader == null) {
				throw e;
			}
			Config.getLog().d("Error loading value. Continuing with sub-loader", e);
		}
		if (result != null || loader == null) {
			markSuccess();
			return;
		}
//		result = create();
		
		loader.addObserver(new Observer<CancelableTask>() {
			@Override
			public void update(CancelableTask args) {
				if (!loader.isDone() || loader.isCanceled()) return;
				if (loader.getError() != null) {
					markFailure(loader.getError());
				}
				if (!loader.isSuccessful()) return; // Should always be true
				result = loader.getResult();
				markSuccess();
			}
		});
		if (isCanceled()) return;
		if (executor == null) {
			loader.run();
		} else {
			executor.execute(loader);
		}
	}
	
	public abstract T create();

	public T getResult() {
		return result;
	}

	@Override
	protected void cancelImpl() {
		if (loader != null) {
			loader.cancel();
//			Loader<T> loaderTemp = loader;
//			loader = null;
//			loaderTemp.cancel();
		}
	}
	
	
}
